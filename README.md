# Grouping clients with 0MQ and nodejs
#### Autor: Jorge Cabrejas Peñuelas

## Objetivo
El objetivo de este proyecto es el diseño y la implementación de un algoritmo para formar un grupo.

## Requisitos
Para poder llevar a cabo el objetivo que se plantea es necesario ejecutar los siguientes comandos:
* Instalar nodejs: *apt-get install nodejs*
* Instalar npm: *apt-get install npm*
* Instalar la librería 0MQ en el directorio de trabajo: apt-get install zeromq@5

## Descripción de la solución
La solución planteada se basa en el siguiente esquema de funciones con sus interacciones:

<img src="figures/funciones.png" width="800">


La implementación se ha optado por 0MQ con sockets REQ/RES y PUB/SUB como muestra el siguiente esquema:

<img src="figures/implementacion.png" width="800">

Los mensajes que se intercambian los clientes y el servidor son como sigue:

<img src="figures/mensajes.png" width="600">

Note que la flecha hacia la derecha indica el mensaje desde el cliente al servidor y la fecha hacia la izquierda indica
el mensaje desde el servidor al cliente. Por otra parte, el color rojo indica los mensajes que se transmiten a través de
los sockets REQ/RES y el color azul los mensajes que se transmiten a través de los sockets PUB/SUB. Los mensajes que se
encuentran entre llaves indican que se ha transmitido un JSON.

## Ejemplos de salidas
A continuación, mostramos un ejemplo de resultado con tres clientes y un servidor:

<img src="figures/salida.png" width="1000">