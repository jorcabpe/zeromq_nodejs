// --------------------------------------------------------
// client.js
// --------------------------------------------------------

//Checking if the input parameters are correct
lenparam = process.argv.length
if (lenparam != 3)
{
    console.log("Incorrect input parameters")
    console.log("Execution mode: node client.js id")
    process.exit(1)
}

//Requires
var zmq = require('zeromq')

//Sockets
var socketToReq = zmq.socket('req')
var socketToSub = zmq.socket('sub')

//Global variables
var array_members = []

//Getting the id
var id = process.argv[2]

//Connecting to the server through a REQ socket
socketToReq.connect("tcp://localhost:5555")

//Connecting to the server through a SUB socket
socketToSub.connect("tcp://localhost:8688")
socketToSub.subscribe("")

//LISTENERS
//REQ
socketToReq.on("message", function(msgServer) {
    if (msgServer == "OK")
    {
        console.log("Message received from server " + msgServer)        
    }
})

//SUB
socketToSub.on("message", function(msgServer) {
    var msgServerJSON = JSON.parse(msgServer)
    if (msgServerJSON.func_name == "update_members")
    {
        update_members(msgServerJSON.members)
    }
})

//Capturing the SININT event
process.on('SIGINT', function() {
	console.log ("Event SIGINT captured")        
    socketToSub.close()
    console.log("SUB Socket closed")
    socketToReq.close()
    console.log ("REQ Socket closed")
    process.exit(0)
})

//SEMANTIC FUNCTIONS
//Adding the id to the group through the server
function add_to_group(id) {
    setTimeout(function() {
        console.log("Sending the id " + id)
        msgToSendJSON = {
            func_name: 'add_to_group',
            id: id
        }
        msgToSend = JSON.stringify(msgToSendJSON)
        socketToReq.send(msgToSend)
    }, 800)
}

add_to_group(id)

//Updating the list the members
function update_members(members)
{
    array_members = members
    var list = ""
    array_members.forEach(function(value){
        list = list + " " + value
    })
    console.log("The list is: (" + list + " )")
}
