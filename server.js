// --------------------------------------------------------
// server.js
// --------------------------------------------------------

//Checking if the input parameters are correct
if (process.argv.length > 2)
{
    console.log("Incorrect input parameters")
    console.log("Execution mode: node server.js")
    process.exit(1)
}

//Requires
var zmq = require('zeromq')

//Sockets
var socketToRes = zmq.socket('rep')
var socketToPub = zmq.socket('pub')

//Global variables
var array_members = []

//Binding into the RES socket
socketToRes.bind('tcp://*:5555', function(err) {
    if (err) {
        console.log(err)
    } else {
        console.log("Listening on 5555...")
    }
})

//Binding into the PUB socket
socketToPub.bind('tcp://*:8688', function(err) {
  if(err)
    console.log(err)
  else
    console.log("Listening on 8688...")
})

//LISTENERS
socketToRes.on('message', function(msgClient) {    

    var msgClientJSON = JSON.parse(msgClient)

    if (msgClientJSON.func_name == "add_to_group")
    {
        console.log("Server receives a request from the client with id " + msgClientJSON.id)
        add_to_group(msgClientJSON.id)
        answer_from_server(msgClientJSON.id)
    }   
})

//Capturing the SININT event
process.on('SIGINT', function() {
    console.log("Event SIGINT captured")
    socketToRes.close()
    socketToPub.close()
    console.log("Sockets closed")
    process.exit(0)
})

//SEMANTIC FUNCTIONS
function update_members(array_members)
{
    console.log("Updating the list of members")
    var msgServerJSON = {
        func_name: "update_members",
        members: array_members
    }
    msgToSend = JSON.stringify(msgServerJSON)
    setTimeout(
        function() {            
            socketToPub.send(msgToSend)
        },
        2000
    )
}

//Adding the id to the group
function add_to_group(id)
{
    console.log("Adding to group the id " + id)
    array_members.push(id)
    update_members(array_members)
}

//Answering to the client in the RES socket
function answer_from_server(id) {
    setTimeout(
        function() {
            console.log("The server answers the message received from the client with id " + id)
            if (socketToRes) 
            {
                socketToRes.send("OK")
            }
            else
            {
                socketToRes.send("Error")
            }
        }
        , 
        4000
    )
}
